# for jcsv
#
# Copyright (c) 2022 ... 2024 2025
#     John McCue
#
# Permission to use, copy, modify, and distribute this software
# for any purpose with or without fee is hereby granted, provided
# that the above copyright notice and this permission notice
# appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
# WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL
# THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT,
# NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# This file should *not* need to be changed unless you need to
# customize some settings for another OS.
# build.sh will take care of the # settings for you.  Execute:
#    ./build.sh --help
# to see compile options
#

SHELL=/bin/sh

#--- where to install
DESTDIR=LOCATION
PRODUCTION=$(DESTDIR)/bin
PRODPMAN=$(DESTDIR)/man
PRODMAN=$(PRODPMAN)/man1
PRODREADH=$(DESTDIR)/share
PRODREADME=$(PRODREADH)/jcsv

#>>>>>>> select desired OS
#--- Linux 64 bit
#LINUX64#WALL=-Wall -Wextra -Wuninitialized -m64
#LINUX64#ETAGS=etags
#LINUX64#GZ=.gz
#LINUX64#NROFF=cat
#LINUX64#OSDESC != uname -smr

#--- Linux 32 bit
#LINUX32#WALL=-Wall
#LINUX32#ETAGS=etags
#LINUX32#GZ=.gz
#LINUX32#NROFF=cat
#LINUX32#OSDESC != uname -smr

#--- BSD 64 bit
#BSD64#WALL=-Wall -Wextra -Wuninitialized -m64
#BSD64#ETAGS=etags
#BSD64#GZ=.gz
#BSD64#NROFF=cat
#BSD64#OSDESC != uname -smr

#--- BSD 32 bit
#BSD32#WALL=-Wall -Wextra -Wuninitialized
#BSD32#ETAGS=etags
#BSD32#GZ=.gz
#BSD32#NROFF=cat
#BSD32#OSDESC != uname -smr

#--- AIX specific
#AIX#WALL=-bnoquiet
#AIX#ETAGS=true
#AIX#GZ=
#AIX#NROFF=nroff -man
#AIX#OSDESC = `uname -a`

#--- should not have to change these
CC=cc
CHMOD=chmod
CP=cp
CTAGS=ctags
ECHO=echo
GZIP=gzip
LINK=cc
#FOUND_JLIB#LIBS=-lj_lib2
#NO_JLIB#LIBS=
#FOUND_JLIB#HJLIB=-DHAVE_JLIB
MV=mv
RM=rm -f
RMDIR=rmdir
STRIP=strip
MKDIR=mkdir

EXE=
OBJ=.o

#--- for prod
#FOUND_JLIB#CFLAGS=-O1 -c $(WALL) $(HJLIB) -IINCJLIB -DOSTYPE="\"$(OSDESC)\""
#FOUND_JLIB#LFLAGS=-O1 $(WALL) -LJLIBLOC -o jcsv $(LIBS)
#NO_JLIB#CFLAGS=-O1 -c $(WALL) -DOSTYPE="\"$(OSDESC)\""
#NO_JLIB#LFLAGS=-O1 $(WALL) -o jcsv $(LIBS)

#--- for valgrind
#    OR for OpenBSD ktrace/kdump
#VALGRIND##FOUND_JLIB#CFLAGS=-O0 -g -c $(WALL) $(HJLIB) -IINCJLIB -DOSTYPE="\"$(OSDESC)\""
#VALGRIND##FOUND_JLIB#LFLAGS=-O0 -g $(WALL) -LJLIBLOC -o jcsv $(LIBS)
#VALGRIND##NO_JLIB#CFLAGS=-O0 -g -c $(WALL) -DOSTYPE="\"$(OSDESC)\""
#VALGRIND##NO_JLIB#LFLAGS=-O0 -g $(WALL) -o jcsv $(LIBS)

#--- continue on
#NO_JLIB#JCC=jcsv_j.c
#NO_JLIB#JCOB=jcsv_j$(OBJ)

ALL_OBJ=jcsv$(OBJ) jcsv_c$(OBJ) jcsv_h$(OBJ) $(JCOB) jcsv_i$(OBJ) jcsv_u$(OBJ)
ALL_C=jcsv.c jcsv_c.c jcsv_h.c $(JCC) jcsv_i.c jcsv_u.c
ALL_H=jcsv.h

#
# do the work
#
all:	ckenv tags $(ALL_OBJ) jcsv.1$(GZ)
	$(LINK) $(LFLAGS) $(ALL_OBJ) $(LIBS)

ckenv:
	echo checking Variable DESTDIR
	test $(DESTDIR)

tags:	$(ALL_H) $(ALL_C)
	-$(CTAGS) $(ALL_H) $(ALL_C)
	-$(ETAGS) $(ALL_H) $(ALL_C)

jcsv.1.gz:	jcsv.1
	-$(RM) jcsv.1.gz
	$(GZIP) jcsv.1

jcsv.1:	jcsv.man
	$(NROFF) jcsv.man > jcsv.1

#--- AIX install(1) is odd compared to BSDs and Linux
install:	all
	-$(MKDIR) $(PRODUCTION)
	-$(MKDIR) $(PRODPMAN)
	-$(MKDIR) $(PRODMAN)
	-$(CHMOD) 755 $(PRODUCTION)
	-$(CHMOD) 755 $(PRODPMAN)
	-$(CHMOD) 755 $(PRODMAN)
	-$(STRIP) jcsv
	$(CP) jcsv $(PRODUCTION)/jcsv
	$(CHMOD) 755 $(PRODUCTION)/jcsv
	$(CP) jcsv.1$(GZ) $(PRODMAN)/jcsv.1$(GZ)
	$(CHMOD) 644 $(PRODMAN)/jcsv.1$(GZ)
	-$(MKDIR) $(PRODREADH)
	-$(MKDIR) $(PRODREADME)
	-$(CHMOD) 755 $(PRODREADH)
	-$(CHMOD) 755 $(PRODREADME)
	$(CP) README.md $(PRODREADME)/README.md
	$(CHMOD) 644 $(PRODREADME)/README.md
	$(CP) LICENSE.md $(PRODREADME)/LICENSE.md
	$(CHMOD) 644 $(PRODREADME)/LICENSE.md

uninstall:	all
	-$(RM) $(PRODUCTION)/jcsv
	-$(RM) $(PRODMAN)/jcsv.1.gz
	-$(RM) $(PRODMAN)/jcsv.1
	-$(RM) $(PRODREADME)/README.md
	-$(RM) $(PRODREADME)/LICENSE.md
	-$(RMDIR) $(PRODREADME)

clean:
	-$(RM) *$(OBJ)
	-$(RM) jcsv$(EXE)
	-$(RM) jcsv.1
	-$(RM) jcsv.1.gz
	-$(RM) TAGS
	-$(RM) tags
	-$(RM) core
	-$(RM) *.core
	-$(RM) a.out
	-$(RM) *.pdb
	-$(RM) *.ilk
	-$(RM) *.bak
	-$(RM) Makefile
	-$(RM) ktrace.out

### END
